import random

#Iterator
class Nest(object):
    """
    Monster's nest
    """
    def __init__(self, *monsters):
        self.__monsters = list(monsters)            

    #return the object that implements __next__ function    
    def __iter__(self):
        return self

    #iterator must implement this function
    def __next__(self):
        try:
            m = random.choice(self.__monsters)
            self.__monsters.remove(m)
            return m
        except IndexError:
            raise StopIteration()