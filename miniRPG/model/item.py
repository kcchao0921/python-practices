class Item(object):
    def __init__(self, name: str):
        self.__name = name

    def __str__(self):
        return self.__name

    def getName(self) -> str:
        return self.__name

class Weapon(Item):
    def __init__(self, name: str, strength: int):
        super().__init__(name)
        self.__strength = strength

    def getStrength(self):
        return self.__strength