import random

from abc import ABCMeta, abstractmethod
from miniRPG.engine.match import Match    #relative import
from .item import Weapon

class Character(metaclass = ABCMeta):
    @abstractmethod
    def _attackedBy(self, attacker, hurt):
        pass

    def __init__(self, name, hp, strength, defense, weapon = "fist"):
        #private
        self.__name = name          
        self.__strength = strength
        self.__defense = defense

        #protected
        self._hp = hp               
        self._weapon = weapon

    def getName(self):
        return self.__name

    def isAlive(self):
        return (self._hp > 0)

    def attack(self, target):
        if(isinstance(self._weapon , Weapon)):
            weaponName = self._weapon.getName()
        else:
            weaponName = self._weapon

        print(self.__name + " attacks " + target.__name + " by " + weaponName)
        target._getHurt(self)

    def _getHurt(self, attacker):
        hurt = attacker.__strength - self.__defense
        if(isinstance(attacker._weapon, Weapon)):
            hurt += attacker._weapon.getStrength()
        if(hurt > 0):
            self._hp -= hurt
        self._attackedBy(attacker, hurt)

class Hero(Character):
    def __init__(self, name, hp, strength, defense, weapon):
        #Character.__init__(self, name, hp, strength, weapon)    #use super() instead
        super().__init__(name, hp, strength, defense, weapon)    
        print(self.getName() + " is on the way to adventure.")

    def _attackedBy(self, attacker, hurt):
        if(not self.isAlive()):
            print(self.getName() + " is dead.")
            return
        print(self.getName() + "'s hp remains %d" %self._hp)

    def burstInto(self, nest):
        for monster in nest:
            Match.begin(self, monster)
            if(not self.isAlive()):
                break

class Monster(Character):
    def _attackedBy(self, attacker, hurt):
        if(not self.isAlive()):
            print(self.getName() + " is dead.")
            self.__mayDropItem()
        elif(hurt > 0):
            print(self.getName() + "'s hp decreased %d." %hurt)
        else:
            print(self.getName() + " didn't get hurt.")

    def __mayDropItem(self):
            if(isinstance(self._weapon, Weapon)):
                if(random.randint(0, 1) == 1):
                    print(str(self._weapon) + " is dropped!")
                