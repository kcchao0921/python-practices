# -*- coding:UTF-8 -*-
#from miniRPG.model import *    #bad practice
from miniRPG.model import character   #from package import module
from miniRPG.model.character import Monster #from module import class (or function, variable)
from miniRPG.model.item import Weapon
from miniRPG.model.maze import Nest

hero = character.Hero("Arthur", 100, 2, 2, Weapon("Excalibur", 5))
hero.leave = lambda : print("The nest is clear.")

nest = Nest(
    Monster(name = "Slime", hp = 6, strength = 1, defense = 1, weapon = "body"),   #keyword argument
    Monster("Goblin", 15, 2, 2, Weapon("club", 1)),
    Monster("Death Knight", 20, 3, 3, Weapon("lance", 3)),
    Monster("Cyclops", 30, 5, 4)   #use default weapon
)

hero.burstInto(nest)
if(hero.isAlive()):
    hero.leave()

#print(hero._Character__name + ":{0}".format(hero._hp))                 #don't do this